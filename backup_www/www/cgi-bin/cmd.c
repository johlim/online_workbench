#include <unistd.h>
extern char **environ;
#define LENBUF	(128)
int main(int argc, char *argv[])
{
	int i;
	char cmdbuf[LENBUF];
	char datbuf[LENBUF];
	memset(cmdbuf,0,LENBUF);
	memset(datbuf,0,LENBUF);
	printf("argc [%d]\n", argc);
	if (argc < 2)
		return 0;
	for (i=0; i < argc; i++)
	printf("%s\n", argv[i]);
	
	sprintf(cmdbuf, "%s", argv[1] );

	sprintf(datbuf, "%s", argv[2] );
	for(i=3; i < argc; i++)
	{
		strcat(datbuf," ");
		strcat(datbuf,argv[i]);
	}

	printf("[%s] [%s] \n", cmdbuf, datbuf);

	switch (argc)
	{
		case 3:
		execlp(cmdbuf, cmdbuf, argv[2],0);
		break;
		case 4:
		execlp(cmdbuf, cmdbuf, argv[2],argv[3],0);
		break;
		case 5:
		execlp(cmdbuf, cmdbuf, argv[2],argv[3],argv[4],0);
		break;
		case 6:
		execlp(cmdbuf, cmdbuf, argv[2],argv[3],argv[4],argv[5],0);
		break;
		case 7:
		execlp(cmdbuf, cmdbuf, argv[3],argv[3],argv[3],argv[3],argv[3],0);
		break;
	}

	return 0;
}
