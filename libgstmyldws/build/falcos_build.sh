# cat facos_build
#!/bin/sh

#변수를 선언하고 초기화 한다.
sb2_config_name="CORTEXA8"
str_prefix="--prefix=/works/project/package/usr"
str_cflags=
str_cortexa8="-march=armv7-a -mtune=cortex-a8 -mcpu=cortex-a8 -mfpu=vfpv3 -mfloat-abi=softfp -ftree-vectorize"
str_sb2_config=

prefix_flag=0
cflags_flag=0

#실행시 인자를 분석해서 변수에 설정한다.
until [ $# -eq 0 ]
do
	argument1=`echo $1 | cut -d'=' -f1`
	#echo $argument
	if [ $argument1 == "--prefix" ]; then
		prefix_flag=1
		str_prefix=$1
		prefix_path=`echo $1 | cut -d'=' -f2`
	elif [ $argument1 == "--mode" ]; then
		cflags_flag=1
		argument2=`echo $1 | cut -d'=' -f2`
		if [ $argument2 == "debug" ]; then
			str_cflags="-g3 -O0"
		elif [ $argument2 == "release" ]; then
			str_cflags="-g0 -O3"
		else
			echo "Invalid mode : $argument2"
		fi
	else
		echo "Invalid argumnt : $argument1"
	fi
	shift
done

echo "prefix=$prefix_flag mode=$cflags_flag"
echo "prefix=$str_prefix cflags=$str_cflags"

#if [ -d $prefix_path ]; then
#echo "[prefix dir] : $str_prefix"
#else
#echo "prefix dir is not exist : $str_prefix"
#echo "usage : *_build.sh --prefix=[prefix_dir] --mode=[debug, release]"
#echo "  example : falcos_build.sh --prefix=/works/project/rootfs --mode=debug"
#echo "  example : falcos_build.sh --mode=release"
#echo "  example : falcos_build.sh --prefix=/works/project/rootfs"
#echo "  default build mode is debug"
#exit
#fi

cd ../
echo "=> changed directory to ($PWD)"
echo "=> # autoconfig start #"
autoreconf --install
cd ./build
echo "=> changed directory to ($PWD)"
echo "=> # configuration start #"
str_sb2_config="../configure $str_prefix CFLAGS=\"$str_cflags $str_cortexa8\""
echo $str_sb2_config
sb2 -t CORTEXA8 $str_sb2_config
echo "=> # make start #"
sb2 -t CORTEXA8 make install

