set -e 
if [ -n "$1" ]; then
 BRANCH=$1
else
 BRANCH="HEAD"
fi
echo ${BRANCH}
