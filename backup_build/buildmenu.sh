#!/bin/sh
if [ -e prototype ] ; then 
	echo "prototype exist"
fi
if [ -e bootloader ] ; then 
	echo "bootloader exist"
fi
if [ -e kernel ] ; then 
	echo "kernel exist"
fi
if [ -e images ] ; then 
	echo "kernel exist"
else
	echo "mkdir images"
	mkdir images
fi

CURHOME=`pwd`
UBOOTDIR='bootloader/u-boot'
KERNELDIR='kernel/kernel-3.4.39'
while :
do 
	echo "Menu"
	echo "1) check source"
	echo "2) set mode "
	echo "3) build"
	echo "4) copy to nfs/tftp"
	echo "5) exit"

	read number
	case $number in 
		1)
			svn export http://opx.iptime.org:2080/opx/repos/trunk/prototype
			svn export http://opx.iptime.org:2080/opx/repos/trunk/bootloader
			svn export http://opx.iptime.org:2080/opx/repos/trunk/kernel
			;;
		2)
			cd $CURHOME
			cd bootloader/u-boot
			find ./ -name "build*" -exec chmod a+rx {} \;
			find ./ -name "mkconfig*" -exec chmod a+rx {} \;
			find ./ -name "mkimage*" -exec chmod a+rx {} \;
			chmod a+rx tools/setlocalversion
			chmod a+rx tools/scripts/make-asm-offsets
			cd ../../
			cd ./kernel/kernel-3.4.39
			find ./ -name "build*" -exec chmod a+rx {} \;
			find ./ -name "mkconfig*" -exec chmod a+rx {} \;
			find ./ -name "mkimage*" -exec chmod a+rx {} \;
			cd $CURHOME
			;;
		3)
			cd $CURHOME
			cd $UBOOTDIR
			./build_config.sh
			./build.sh

			cd $CURHOME
			cd $KERNELDIR
			./build_config.sh
			./build.sh
			cd $CURHOME
			;;
		4)
			pwd
			;;
		5)	
			exit
			;;
		*)
			echo "error command"
			;;
	esac
	echo
done

