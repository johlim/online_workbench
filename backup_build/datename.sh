#!/bin/sh
if [ $# = 0 ]; then
	echo "arg count is 0"
	srcfolder="ramdisk"
else
	srcfolder="$1"
fi
#exit
tarfolder="${srcfolder}.$(date '+%Y%m%d').tgz"

if [ -e $tarfolder ]; then
tarfolder="${srcfolder}.$(date '+%Y%m%d%H%M.%S').tgz"
fi

tar -cvjf "$tarfolder" -X exclude.lst "$srcfolder"
#cp -a "$srcfolder" "$tarfolder" 
